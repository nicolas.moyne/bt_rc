// Ce code applique strictement les instructions envoyées par le port série
// Une modification intéressante pourrait être d'utiliser Software.sérial pour ne pas monopoliser l'USB


// UPDATE et FIN_UPDATE pour savoir quelles données correspondent à quel axe
// On définit les numéros des pins de sortie
// MODE est le pin de sortie de la led de failsafe
#define UPDATE 100
#define FIN_UPDATE 101
#define THROTTLE 3
#define YAW 5
#define PITCH 6
#define ROLL 9
#define MODE 12


void setup() {
  // put your setup code here, to run once:
  char c;
  // même baudrate que dans la librairie
  Serial.begin(9600);
  // on vide le port série
  Serial.flush();

  // On spécifie que les ports sont utilisés en sortie
  pinMode(THROTTLE, OUTPUT);
  pinMode(YAW, OUTPUT);
  pinMode(PITCH, OUTPUT);
  pinMode(ROLL, OUTPUT);
  pinMode(MODE, OUTPUT);

  // On fait clignoter la led de failsafe au départ pour montrer que l'on a fini le setup
  
  digitalWrite(MODE, HIGH);
  delay(200);
  digitalWrite(MODE, LOW);

}

void loop() {
  // put your main code here, to run repeatedly:

  char commande;
  // Si on recoit une commande
  if (Serial.available()>0) {


     // On récupère la commande
     commande = Serial.read();

     // On vérifie que la commande corresponde à une demande d'update
     if (commande==UPDATE) 
     {
      // On appelle la fonction d'update
      update_pins();
     }  
  }
}




void update_pins()
{

  // Les différentes variables utilisées pour stocker les informations envoyées par l'ordinateur
  char t;
  char p;
  char r;
  char y;
  char failsafe;
  char mode;
  char fin;

  // on attend d'avoir recu toutes les commandes
  while(Serial.available()< 7)
  {
    // tant que l'on n'a pas recu toutes les commandes on ne fait rien
  }
  // on lit toutes les commandes
  t = Serial.read();
  p = Serial.read();
  r = Serial.read();
  y = Serial.read();
  failsafe = Serial.read();
  mode = Serial.read();
  fin = Serial.read();

  // Selon le protocole, la dernière information envoyées doit correspondre à FIN_UPDATE
  if(fin==FIN_UPDATE){

    // Si on doit entrer en mode failsafe
    if (failsafe==1){
      // On met les moteurs à 0
      analogWrite(THROTTLE, 0);
      analogWrite(PITCH, 0);
      analogWrite(YAW, 0);
      analogWrite(ROLL, 0);
      // On entre dans une boucle infinie de clignotement
      // Seul l'appui sur le bouton de reset relancera le programme et arrêtera cette boucle
      while(true){
        digitalWrite(MODE, HIGH);
        delay(200);
        digitalWrite(MODE, LOW);
        delay(200);
      }
    }

    // Si on est pas en mode failsafe on met à jour les pins
    analogWrite(THROTTLE, t);
    analogWrite(PITCH, p);
    analogWrite(YAW, y);
    analogWrite(ROLL, r);
    digitalWrite(MODE, mode);
  }
  // Si le message n'est pas de la bonne forme on arrive directement ici et on ne fait rien
  return;
}
