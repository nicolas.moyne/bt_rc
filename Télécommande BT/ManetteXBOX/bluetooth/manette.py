""" UTILISATION DU MODULE 

Ce module simule une radio commande de drone en mode 2.
Pour l'utiliser, relier l'arduino ainsi 
    - Throttle : pin 3
    - Yaw : pin 5
    - Pitch : pin 6
    - Roll : pin 9
    - Led indicatrice (du failsafe) : pin 12
    - Pin RX/TX du module BT - pin TX/RX (noter l'ordre) de l'arduino


Etapes pour l'utilisation :
    - brancher les pins comme ci-dessus
    - alimenter l'arduino 
    - la led de failsafe émettra un clignotement de 0.2s
    - connecter l'ordinateur au BT04 (module BT) : dans les paramètres avancés (à faire une fois)
    - consulter le numéro de port COM correspondant au BT04 et le modifier ci-dessous
"""

# On peut vérifier sur quel port est connecté le bluetooth via Paramètres -> Bluetooth -> Paramètres avancés -> Ports COM
# On cherche la device BT-04A (port sortant, rq : deux ports, un pour RX, un pour TX)
# Sur MAC ou Linux, la nomenclature des ports est différente
port = "COM13"

"""
    - lancer le programme manette.py
    - la télécommande est fonctionnelle

Fonctionnalités :
    - télécommande mode 2 classique
    - appuyer sur A : failsafe
        -tous les moteurs s'éteignent et la led de failsafe clignote
        - pour désactiver le failsafe : cliquer sur le bouton reset de l'arduino
        - la télécommande est à nouveau fonctionnelle
    - appuyer sur B : allume/éteint la led de failsafe
    - appuyer sur Y : met les moteurs à 0 et déconnecte la télécommande
"""



# Module pour gérer la manette de XBox
import pygame

# Module pour le temps
import time

# Module pour gérer l'envoie des commandes en Arduino
# Ce module utilise Serial, mais permet de masquer les manipulations effectuées sur les ports séries.
from commandes_ardu import Arduino



## INITIALISATION DE L'ARDUINO
# On démarre la liaison série avec Arduino sur le port spécifié (cf commande_ardu.py)
ard = Arduino(port)

pygame.init()

# Ce que l'on appelle ici "joystick" désigne toute la manette et pas juste un des joysticks sur la manette
joystick = pygame.joystick.Joystick(0)


# Récupère la position des axes et des boutons de la manette
def recupere_manette():
    # sert à actualiser les valeurs des axes et des boutons de l'objet python "joystick" par rapport à la manette réelle
    pygame.event.get()
    # get_axis : position sur un axe (on en utilise 4) entre -1 et 1
    # get_button : un bouton, 0 ou 1
    y = joystick.get_axis(0)
    t = joystick.get_axis(1)
    r = joystick.get_axis(2)
    p = joystick.get_axis(3)
    a = int(joystick.get_button(0))
    b = int(joystick.get_button(1))
    c = int(joystick.get_button(3))
    return (t, p, r, y, a, b, c)

# les axes sont entre -1 et 1 et inversés (par exemple, THROTTLE au max donne -1). On les veut entre 0 et 100.
def convertir(x):
    v = int((1-x)*50)
    return v


# on envoie des mises à jours espacées de delai secondes
t_prev = time.time()
delai = 0.05


# Pour assurer que la led failsafe change d'état de manière stable à l'appui de B.
# Stratégies : on change l'état au max toutes les 0.2s
# Mode : état de la led 
delai_changement_b = 0.2
last_b = 0
time_change_b = time.time() 
mode = 0

while True:
    t_act = time.time()
    if t_act - t_prev > delai:

        # on acquiert les positions de la manette
        t, p, r, y, a, b, c = recupere_manette()

        # on met les axes entre 0 et 100
        val_t = convertir(t)
        val_p = convertir(p)
        val_r = convertir(r)
        val_y = convertir(y)

        # presser le bouton A fait passer le drone en failsafe
        # si on presse le bouton Y, on passe le drone en mode failsafe avant de déconnecter la télécommande
        failsafe = a or c


        # si on presse B et si l'état de la led ("mode") n'a pas été modifié on change cet état
        if b==1:
            t_b = time.time()
            if t_b - time_change_b > delai_changement_b:
                mode = int((1-mode))
                time_change_b = t_b

        if failsafe :
            print("failsafe")


        # on envoie à l'Arduino les ordres sur les différents axes, le failsafe et l'état de la led
        ard.update(val_t, val_p, val_r, val_y, failsafe, mode)

        # si Y a été pressé, on ferme la liaison
        if c==1:
            ard.close()
            print("Télécommande déconnectée")
            break

        

        t_prev = t_act
            
    




    







"""


# Ports qui serviront pour transmettre le signal PWM depuis l'arduino
pins = {"throttle" : 3, "pitch" : 5, "roll" : 6, "yaw" : 9, "bouton" : 12}

etat = ard.LOW


# OUTPUT : pour pouvoir envoyer un signal
for pin in pins.values():
    ard.pinMode(pin, ard.OUTPUT)


## INITIALISATION DE PYGAME

pygame.init()

# Ce que l'on appelle ici "joystick" désigne toute la manette et pas juste un des joysticks sur la manette
joystick = pygame.joystick.Joystick(0)

# Différents axes pour controller le drone
name = ['yaw', 'throttle','roll', 'pitch']





## DEFINITION DES FONCTIONS DE COMMUNICATION


# Pour tester si la MANETTE fonctionne
def envoie_commande_test(commande):
    axe, perc = commande
    nom = name[axe]

    print(nom, ' : ', perc, '%')

    return


# vraie fonction de communication
def envoie_commande(commande):
    # axe : sur la manette, chaque joystick a deux axes (donc axes de 0 à 3)
    # les axes peuvent prendre une valeur, ici perc, entre -1 et 1
    axe, perc = commande

    # augmente le nombre de lignes mais simplifie le débogage : on travaille avec le nom de l'axe et pas avec son numero
    nom = name[axe]

    # On regarde sur quel pin (de l'Arduino) il va falloir écrire 
    pinWrite = pins[nom]


    # Sur une vraie télécommande, l'axe de throttle ne revient pas au milieu en l'absence de contrainte.
    # On peut minimiser ce problème en mettant à 0% la valeur de throttle quand elle est sous le milieu
    if nom == "throttle":
        if perc > 0:
            # La modulation PWM de l'arduino fonctionne en 8 bits
            val = int((perc)*(255/100))
        else:
            val = 0


    # sur les autres axes, on a pas le même problème
    else:
        val = int((perc +100)*(255/200))

    # Fonction de l'arduino.
    # Ecrit un signal PWM (0 -> 0% DutyCycle, 255 -> 100% DutyCycle)
    ard.analogWrite(pinWrite, val)



## Boucle de corps

while True:

    # Pygame provoque un évènement dès que les positions sur le joystick sont modifiées
    # Si l'évènement consiste en un joystick bougé, on récupère son axe et sa nouvelle valeur
    # On envoie cette commande au drone

    for event in pygame.event.get(): # get the events (update the joystick)
        if event.type == pygame.JOYAXISMOTION:
            axe, valeur = event.axis, event.value
            perc = -int(valeur*100) # Les valeurs vont entre 1 et -1 et pas -1 et 1


            commande = (axe, perc)

            envoie_commande(commande)

    # Appuyer sur le bouton A éteint la télécommande

    if joystick.get_button(0):
        ard.close()
        print("Stop")
        break

    # Appuyer sur B modifie l'état du pin "Bouton" (a des fins de test)
    # HIGH et LOW représentent les états logiques 0 et 1

    if joystick.get_button(1):
        if etat==ard.HIGH:
            etat=ard.LOW
        else:
            etat = ard.HIGH

        # Digital : écrit 0 ou 1
        ard.digitalWrite(pins["bouton"], etat)


"""