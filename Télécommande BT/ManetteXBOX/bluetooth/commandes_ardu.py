# -*- coding: utf-8 -*-

# Module de communication BT :
#   - Ne permet plus de retour de l'Arduino
#   - Ne vérifie pas que l'Arduino reste en ligne 


# Ce module sert majoritairement à cacher les communications séries 
import serial
                

# Tout repose sur une classe Arduino qui définit un protocole de communication simple

"""
PROTOCOLE DE COMMUNICATION :
    - Envoyer 100 (UPDATE)
    - Transmettre dans l'ordre les données des 4 axes, du failsafe et de la led
    - Envoyer 101 (FIN_UPDATE)
    - Si les information ne sont pas envoyées dans cet ordre, l'Arduino ne les prend pas en compte
    - Pas de retour de la part de l'Arduino
"""
class Arduino():

    # Lorsqu'on crée un arduino
    def __init__(self,port):

        # Il n'est pas très facile de savoir quel baudrate utiliser
        # Le baudrate doit absolument correspondre à celui indiqué sur l'arduino
        self.ser = serial.Serial(port,baudrate=9600)


        # Pour vérifier la bonne transmission des données
        self.UPDATE = 100
        self.FIN_UPDATE = 101
        


    # Pour fermer l'Arduino    
    def close(self):
        self.ser.close()

    # On écrit juste sur le port série
    # Encode et chr servent à transformer les entiers en bits transmissibles
   
    def update(self, val_t, val_p, val_r, val_y, failsafe, mode):
        self.ser.write(chr(self.UPDATE).encode())
        self.ser.write(chr(val_t).encode())
        self.ser.write(chr(val_p).encode())
        self.ser.write(chr(val_r).encode())
        self.ser.write(chr(val_y).encode())
        self.ser.write(chr(failsafe).encode())
        self.ser.write(chr(mode).encode())
        self.ser.write(chr(self.FIN_UPDATE).encode())
    
